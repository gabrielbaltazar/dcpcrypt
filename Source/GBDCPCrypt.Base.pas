unit GBDCPCrypt.Base;

interface

uses
  GBDCPCrypt.Interfaces;

type TGBDCPCryptBase = class(TInterfacedObject, IGBDCPCrypt)

  protected
    FKey: string;

  public
    function Crypto(Value: string): string; virtual; abstract;
    function DeCrypto(Value: string): string; virtual; abstract;
    function Key(Value: string): IGBDCPCrypt;

    class function New: IGBDCPCrypt;
end;

implementation

{ TGBDCPCryptBase }

function TGBDCPCryptBase.Key(Value: string): IGBDCPCrypt;
begin
  result := Self;
  FKey := Value;
end;

class function TGBDCPCryptBase.New: IGBDCPCrypt;
begin
  result := Self.Create;
end;

end.
