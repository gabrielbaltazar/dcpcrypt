unit GBDCPCrypt.Factory;

interface

uses
  GBDCPCrypt.Interfaces,
  GBDCPCrypt.RC4;

type TGBDCPCryptFactory = class(TInterfacedObject, IGBDCPCryptFactory)

  public
    function createCryptDefault: IGBDCPCrypt;
    function createCryptRC4: IGBDCPCrypt;

    class function New: IGBDCPCryptFactory;
end;

implementation

{ TGBDCPCryptFactory }

function TGBDCPCryptFactory.createCryptDefault: IGBDCPCrypt;
begin
  result := createCryptRC4;
end;

function TGBDCPCryptFactory.createCryptRC4: IGBDCPCrypt;
begin
  result := TGBDCPCryptRC4.New;
end;

class function TGBDCPCryptFactory.New: IGBDCPCryptFactory;
begin
  result := Self.Create;
end;

end.
