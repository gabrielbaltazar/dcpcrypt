unit GBDCPCrypt.RC4;

interface

uses
  System.SysUtils,
  GBDCPCrypt.Interfaces,
  GBDCPCrypt.Base,
  DCPrc4,
  DCPsha1,
  DCPsha256,
  DCPsha512;

type TGBDCPCryptRC4 = class(TGBDCPCryptBase, IGBDCPCrypt)

  private
    function createCipher: TDCP_rc4;

  public
    function Crypto(Value: string): string; override;
    function DeCrypto(Value: string): string; override;
end;

implementation

{ TGBDCPCryptRC4 }

function TGBDCPCryptRC4.createCipher: TDCP_rc4;
begin
  result := TDCP_rc4.Create(nil);
  try
    result.InitStr(FKey, TDCP_sha512);
  except
    Result.Free;
    raise;
  end;
end;

function TGBDCPCryptRC4.Crypto(Value: string): string;
var
  Cipher: TDCP_rc4;
begin
  if Value.Trim.IsEmpty then
    Exit(Value);

  Cipher:= createCipher;
  try
    result := Cipher.EncryptString(Value);
  finally
    Cipher.Burn;
    Cipher.Free;
  end;
end;

function TGBDCPCryptRC4.DeCrypto(Value: string): string;
var
  Cipher: TDCP_rc4;
begin
  if Value.Trim.IsEmpty then
    Exit(Value);

  Cipher := createCipher;
  try
    result := Cipher.DecryptString(Value);
  finally
    Cipher.Burn;
    Cipher.Free;
  end;
end;

end.
