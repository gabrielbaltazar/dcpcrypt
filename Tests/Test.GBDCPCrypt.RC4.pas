unit Test.GBDCPCrypt.RC4;

interface

uses
  DUnitX.TestFramework,
  GBDCPCrypt.Interfaces,
  Test.GBDCPCrypt.Base;

type
  [TestFixture]
  TTestGBDCPCryptRC4 = class(TTestGBDCPCryptBase)

  private
    FCrypt: IGBDCPCrypt;

    FCriptografado   : string;
    FDeCriptografado : string;

  public
    [Setup] procedure Setup; override;

    [Test] procedure CryptoStringVazia;
    [Test] procedure DeCryptoStringVazia;
    [Test] procedure Crypto;
    [Test] procedure DeCrypto;
    [Test] procedure CryptoCaracteresEspeciais;
  end;

implementation

{ TTestGBDCPCryptRC4 }

procedure TTestGBDCPCryptRC4.Crypto;
var
  chave: string;
begin
  chave := FCrypt.Crypto(FDeCriptografado);
  Assert.AreEqual(chave, FCriptografado);
end;

procedure TTestGBDCPCryptRC4.CryptoCaracteresEspeciais;
var
  chave: String;
  resultado: string;
begin
  chave := '� coisa do vov� e da vov�.';
  resultado := FCrypt.Crypto(chave);
  resultado := FCrypt.DeCrypto(resultado);

  Assert.AreEqual(chave, resultado);
end;

procedure TTestGBDCPCryptRC4.CryptoStringVazia;
var
  resultado: string;
begin
  resultado := FCrypt.Crypto('');

  Assert.IsEmpty(resultado);
end;

procedure TTestGBDCPCryptRC4.DeCrypto;
var
  chave: string;
begin
  chave := FCrypt.DeCrypto(FCriptografado);
  Assert.AreEqual(chave, FDeCriptografado);
end;

procedure TTestGBDCPCryptRC4.DeCryptoStringVazia;
var
  resultado: string;
begin
  resultado := FCrypt.DeCrypto(' ');
  Assert.AreEqual(' ', resultado);
end;

procedure TTestGBDCPCryptRC4.Setup;
begin
  inherited;
  FCrypt := Self.Factory.createCryptRC4;
  FCrypt.Key('TESTE');

  FDeCriptografado := 'gabrielbaltazar';
  FCriptografado   := FCrypt.Crypto(FDeCriptografado);
end;

initialization
  TDUnitX.RegisterTestFixture(TTestGBDCPCryptRC4);

end.
