unit Test.GBDCPCrypt.Base;

interface

uses
  GBDCPCrypt.Interfaces,
  GBDCPCrypt.Factory;

type TTestGBDCPCryptBase = class

  protected
    Factory: IGBDCPCryptFactory;

  public
    [Setup] procedure Setup; virtual;
end;

implementation

{ TTestGBDCPCryptBase }

procedure TTestGBDCPCryptBase.Setup;
begin
  Factory := TGBDCPCryptFactory.New;
end;

end.
